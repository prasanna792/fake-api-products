const form = document.getElementById('form')
const firstName = document.getElementById('first-name');
const lastName = document.getElementById('last-name');
const email = document.getElementById('email');
const password = document.getElementById('password');
const passwordRepeat = document.getElementById('password-repeat');
const termsConditions = document.getElementById('terms-sec');
const termsErrorMsg = document.querySelector('.terms-error-msg')
const successMsg = document.querySelector('.sign-up-done')
const signUpSec = document.querySelector('.sign-up-container');
const existingUsers = JSON.parse(localStorage.getItem('users')) || [];


function errorOccurred(element, text) {
  element.nextElementSibling.textContent = text
  element.nextElementSibling.style.color = 'red'
  element.style.border = '1px solid red';
}

function borderGreen(element) {
  element.nextElementSibling.textContent = ""
  element.style.border = '1px solid green'
}


function checkName(name) {
  const checkCharacters = ['~', '!', '@', '#', '$', '%', '*', '(', ')', '+', '=', ':', '^', '[', ']', '_', '-', ';', '>', '<', '/', '?', '|', '\\', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

  for (const char of checkCharacters) {
    if (name.includes(char)) {
      return true;
    }
  }
  return false;
}


function checkEmail(email) {

  if (email.match(/\s/g)) {
    return true;
  }
  if (email.indexOf('@') === -1) {
    return true;
  }

  const [localPart, domainPart] = email.split('@');

  if (localPart.length === 0 || domainPart.length === 0) {
    return true;
  }

  const domainParts = domainPart.split('.');
  if (domainParts.length < 2 || domainParts.some(part => part.length === 0)) {
    return true;
  }

  return false;
}



function checkPassword(password) {
  const checkCharacters = ['~', '!', '@', '#', '$', '%', '*', '(', ')', '+', '=', ':', '^', '[', ']', '_', '-', ';', '>', '<', '/', '?', '|', '\\'];
  const numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  const upperCaseAlphabets = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  let hasSpecialCharacter = false;
  let hasNumber = false;
  let hasUppercaseAlphabet = false;

  if (password.match(/\s/g)) {
    return true;
  }

  if (password.length < 8 || password.length > 16) {
    return true;
  }

  for (const char of password) {
    if (checkCharacters.includes(char)) {
      hasSpecialCharacter = true;
      break;
    }
  }

  for (const char of password) {
    if (numbers.includes(char)) {
      hasNumber = true;
      break;
    }
  }

  for (const char of password) {
    if (upperCaseAlphabets.includes(char)) {
      hasUppercaseAlphabet = true;
      break;
    }
  }

  if (!(hasNumber && hasSpecialCharacter && hasUppercaseAlphabet)) {
    return true;
  } else {
    return false;
  }

}


form.addEventListener('submit', (event) => {
  event.preventDefault();
  let checkList = false;
  if (firstName.value === '' || checkName(firstName.value)) {
    errorOccurred(firstName, 'Please enter name using letters only')
    checkList = true
  } else {
    borderGreen(firstName);
  }

  if (lastName.value === '' || checkName(lastName.value)) {
    errorOccurred(lastName, 'Please enter name using letters only')
    checkList = true
  } else {
    borderGreen(lastName)
  }

  if (email.value == '' || checkEmail(email.value)) {
    errorOccurred(email, 'Invalid email address')
    checkList = true
  } else {
    borderGreen(email)
  }

  if (password.value == '' || checkPassword(password.value)) {
    errorOccurred(password, 'Your password must be at least 8 to 16 characters and it should include atleast one special character,number,uppercase alphabet')
    checkList = true
  } else {
    borderGreen(password)
  }

  if ((password.value === passwordRepeat.value && passwordRepeat.value !== '') && !checkPassword(passwordRepeat.value)) {
    borderGreen(passwordRepeat)
  } else {
    errorOccurred(passwordRepeat, 'Password must be same')
    checkList = true
  }


  if (!termsConditions.checked) {
    termsErrorMsg.textContent = 'Please accept terms and conditions';
    termsErrorMsg.style.color = 'red';
    checkList = true
  }
  if (!checkList) {
    successMsg.style.display = 'block';
    signUpSec.style.display = 'none';
    console.log(!checkList, "check")

    console.log('First Name:', firstName.value);
    console.log('Last Name:', lastName.value);
    console.log('Email:', email.value);
    console.log('Password:', password.value);

    
    const userDetails = {
      firstName: firstName.value,
      lastName: lastName.value,
      email: email.value,
      password: password.value
    };

    existingUsers.push(userDetails);

    localStorage.setItem('users', JSON.stringify(existingUsers));

    form.reset();
  }
});



firstName.addEventListener('keyup', () => {
  if (firstName.value === '' || checkName(firstName.value) || firstName.value.length < 3) {
    errorOccurred(firstName, 'Please enter name using letters only,it does not includes special characters and numbers');
  } else {
    borderGreen(firstName);
  }
});

lastName.addEventListener('keyup', () => {
  if (lastName.value === '' || checkName(lastName.value) || lastName.value.length < 3) {
    errorOccurred(lastName, 'Please enter name using letters only,it does not includes special characters and numbers');
  } else {
    borderGreen(lastName);
  }
});

email.addEventListener('keyup', () => {
  if (email.value == '' || checkEmail(email.value)) {
    errorOccurred(email, 'Invalid email address,it should include @ character,no spaces in between of characters');
  } else {
    borderGreen(email);
  }
});

password.addEventListener('keyup', () => {
  if (password.value == '' || checkPassword(password.value)) {
    errorOccurred(password, 'Your password must be at least 8 to 16 characters and it should include atleast one special character,number,uppercase alphabet');
  } else {
    borderGreen(password);
  }
});

passwordRepeat.addEventListener('keyup', () => {
  if ((password.value === passwordRepeat.value && passwordRepeat.value !== '') && !checkPassword(passwordRepeat.value)) {
    borderGreen(passwordRepeat);
  } else {
    errorOccurred(passwordRepeat, 'Password must be same');
  }
});

termsConditions.addEventListener('change', () => {
  if (termsConditions.checked) {
    termsErrorMsg.textContent = '';
  }
});





