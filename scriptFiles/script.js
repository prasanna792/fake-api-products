
fetch('https://fakestoreapi.com/products')
    .then((response) => {
        if (!response.ok) {
            console.log('Fetching data failed');
            errorMessage();
            removeLoader();
            throw new Error('Fetching data failed');
        } else {
            return response.json();
        }
    })
    .then((data) => {
        if (data.length < 1) {
            console.log("No products");
            noProducts()
        } else {
            showheading();
            removeLoader();
            console.log(data);
            allProducts(data);
        }
    })
    .catch((err) => {
        errorMessage();
        removeLoader();

        console.error(err, 'Fetching error');
    });


function removeLoader() {
    const loader = document.querySelector('.lds-dual-ring');
    loader.remove();
}

function showheading() {
    const categoryContainer = document.querySelector('.category-container');
    categoryContainer.style.display = 'flex';
}

function noProducts() {
    const noProducts = document.querySelector('.noProducts');
    noProducts.style.display = 'block';
}

function errorMessage() {
    const failed = document.querySelector('.error-message');
    failed.style.display = 'block';
}




function createProductCard(product) {

    let card = document.createElement('div');
    card.classList.add('cards');

    let category = document.createElement('p');
    category.textContent = product.category;
    category.classList.add('category');
    card.appendChild(category);


    let imgDiv = document.createElement('div');
    imgDiv.classList.add('img-container')
    let img = document.createElement('img');
    img.src = product.image;
    imgDiv.appendChild(img);
    card.appendChild(imgDiv);


    let details = document.createElement('div');
    details.classList.add("details");

    let title = document.createElement('h4');
    title.textContent = product.title;
    title.classList.add('title');

    let description = document.createElement('p');
    description.classList.add('description');
    description.textContent = product.description.slice(0, 75) + '...';

    let ratingPriceDiv = document.createElement('div');
    ratingPriceDiv.classList.add('rating-price-container');
    let rating = document.createElement('p');
    rating.textContent = product.rating.rate;
    rating.classList.add('rating');
    let ratingStar = document.createElement('span');

    const ratingStarIcon = document.createElement('i');
    ratingStarIcon.classList.add('fa-solid', 'fa-star');

    ratingStar.append(ratingStarIcon);
    rating.append(ratingStar);

    let ratingCountDiv = document.createElement('div');
    ratingCountDiv.classList.add("rating-count-container");
    let ratingCount = document.createElement('span');
    ratingCount.classList.add("rating-count");
    ratingCount.textContent = `${product.rating.count} ratings`;

    ratingCountDiv.append(rating, ratingCount);
    ratingPriceDiv.appendChild(ratingCountDiv);

    let price = document.createElement('h3');
    price.classList.add('price');
    price.textContent = '$' + product.price;
    ratingPriceDiv.appendChild(price);

    details.append(title, description, ratingPriceDiv);

    card.append(details);
    return card;
}

function allProducts(products) {
    const productsDetails = document.querySelector('.all-products');

    displayProducts(products);

    const search = document.querySelector('.search');
    search.addEventListener('keyup', handleSearch);

    const filterButtons = document.querySelectorAll('.filter-button');
    filterButtons.forEach(button => {
        button.addEventListener('click', () => {
            const selectedCategory = button.getAttribute('data-category');
            if (selectedCategory === "all") {
                displayProducts(products);
            } else {
                const filteredProducts = products.filter(product => {
                    return product.category === selectedCategory
                });
                displayProducts(filteredProducts);
            }
        });
    });

    function handleSearch() {
        const searchTerm = search.value.toLowerCase().trim();
        const filteredProducts = products.filter(product => {
            return product.title.toLowerCase().includes(searchTerm)
        });
        displayProducts(filteredProducts);
    }

    function displayProducts(productsToDisplay) {
        productsDetails.textContent = '';
        productsToDisplay.forEach(product => {
            const card = createProductCard(product);
            card.addEventListener('click', () => productDetails(product));
            productsDetails.appendChild(card);
        });
    }


}




const productsContainer = document.querySelector('.product-content');
const modalDiv = document.createElement('div');
modalDiv.classList.add('modal');
modalDiv.setAttribute('id', 'myModal');
const modalContentDiv = document.createElement('div');
modalContentDiv.classList.add('modal-content');

const closeIcon = document.createElement('span');
closeIcon.textContent = 'x';
closeIcon.classList.add('close');
closeIcon.addEventListener('click', (event) => {
    event.stopPropagation();
    modalDiv.style.display = 'none';
});

modalDiv.appendChild(modalContentDiv);
modalDiv.appendChild(closeIcon);
productsContainer.appendChild(modalDiv);

function productDetails(product) {
    let card = eachProduct(product)
    modalContentDiv.textContent = '';
    modalContentDiv.append(card);

    modalDiv.style.display = 'block';
}

function eachProduct(product) {
    const mainDiv = document.createElement('div');
    mainDiv.classList.add('modal-details');

    let imgDiv = document.createElement('div');
    imgDiv.classList.add('modal-img-container')
    let img = document.createElement('img');
    img.src = product.image;

    imgDiv.append(img);

    let textDiv = document.createElement('div');
    textDiv.classList.add('modal-text-details')
    const title = document.createElement('h2');
    title.classList.add('modal-title');
    title.textContent = product.title;

    const category = document.createElement('span');
    category.classList.add('modal-category');
    category.textContent = product.category;

    const description = document.createElement('p');
    description.classList.add('modal-description');
    description.textContent = product.description;

    const price = document.createElement('h3');
    price.classList.add('modal-price');
    price.textContent = `$${product.price}`;

    const rating = document.createElement('span');
    rating.classList.add('modal-rating');
    rating.textContent = `${product.rating.rate}`;

    const ratingStar = document.createElement('span');
    const ratingStarIcon = document.createElement('i');
    ratingStarIcon.classList.add('fa-solid', 'fa-star');
    ratingStar.append(ratingStarIcon);
   
    const ratingDiv = document.createElement('div');
    ratingDiv.classList.add('modal-rating-container');

    rating.appendChild(ratingStar);

    const totalRatings = document.createElement('h3');
    totalRatings.classList.add('modal-rating-count');
    totalRatings.textContent = `${product.rating.count} ratings`;

    ratingDiv.append(rating, totalRatings);

    const ratePriceDiv = document.createElement('div');
    ratePriceDiv.classList.add('modal-rate-price-container');

    ratePriceDiv.append(price, ratingDiv);

    textDiv.append(category, title, description, ratePriceDiv);

    mainDiv.append(imgDiv, textDiv);

    return mainDiv;


}


