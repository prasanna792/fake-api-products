const loginForm = document.getElementById('login-form');
const email = document.getElementById('email');
const password = document.getElementById('password');
const errorMessage = document.getElementsByClassName('err-message');
const successMsg = document.querySelector('.sign-up-done');
const existingUsers = JSON.parse(localStorage.getItem('users')) || [];



function errorOccurred(element, text) {
    element.nextElementSibling.textContent = text;
    element.nextElementSibling.style.color = 'red';
    element.style.border = '1px solid red';
}

function borderGreen(element) {
    element.nextElementSibling.textContent = "";
    element.style.border = '1px solid green';
}


function checkEmail(email) {

    if (email.match(/\s/g)) {
        return true;
    }
    if (email.indexOf('@') === -1) {
        return true;
    }

    const [localPart, domainPart] = email.split('@');

    if (localPart.length === 0 || domainPart.length === 0) {
        return true;
    }

    const domainParts = domainPart.split('.');
    if (domainParts.length < 2 || domainParts.some(part => part.length === 0)) {
        return true;
    }

    return false;
}



function checkPassword(password) {
    const checkCharacters = ['~', '!', '@', '#', '$', '%', '*', '(', ')', '+', '=', ':', '^', '[', ']', '_', '-', ';', '>', '<', '/', '?', '|'];
    const numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const upperCaseAlphabets = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    let hasSpecialCharacter = false;
    let hasNumber = false;
    let hasUppercaseAlphabet = false;

    if (password.match(/\s/g)) {
        return true;
    }

    if (password.length < 8 || password.length > 16) {
        return true;
    }

    for (const char of password) {
        if (checkCharacters.includes(char)) {
            hasSpecialCharacter = true;
            break;
        }
    }

    for (const char of password) {
        if (numbers.includes(char)) {
            hasNumber = true;
            break;
        }
    }

    for (const char of password) {
        if (upperCaseAlphabets.includes(char)) {
            hasUppercaseAlphabet = true;
            break;
        }
    }

    if (!(hasNumber && hasSpecialCharacter && hasUppercaseAlphabet)) {
        return true;
    } else {
        return false;
    }

}


loginForm.addEventListener('submit', (event) => {
    event.preventDefault();
    let checkList = false;

    const findUserEmailAndPassword = existingUsers.find((user) => {
        return user.email === email.value && user.password === password.value;
    });

    if (!findUserEmailAndPassword) {
        errorMessage[0].textContent = 'Incorrect email and password,please enter correct credentials';
        errorMessage[0].style.color = 'red';
    }

    if (email.value === '' && password.value === '') {
        errorMessage[0].textContent = 'Please enter email and password';
        errorMessage[0].style.display = 'block';
        errorMessage[0].style.color = 'red';
    }

    if (!checkList && findUserEmailAndPassword) {
        loginForm.style.display = 'none';
        successMsg.style.display = 'block';
    }


})


email.addEventListener('keyup', () => {

    if (email.value == '' || checkEmail(email.value)) {
        errorOccurred(email, 'Invalid email address,it should includes "@" character,no spaces in between of characters');
    } else {
        borderGreen(email);
    }
});

password.addEventListener('keyup', () => {

    if (password.value == '' || checkPassword(password.value)) {
        errorOccurred(password, 'Your password must be at least 8 to 16 characters and it should include atleast one special character,number,uppercase alphabet');
    } else {
        borderGreen(password);
    }
});


